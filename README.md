# TrixtaFirmware

> **Nerves based Trixta firmware builder**.

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v1.4%20adopted-ff69b4.svg)](code-of-conduct.md)
[![Maintenance](https://img.shields.io/maintenance/yes/2019.svg)]()

## Trixta Firmware Build

### 1. Git Submodule Setup

This project uses Git Submodules to pull in the main `trixta_space` app and
it's dependencies. On a fresh clone you can install the submodules by doing...

```sh
$ git submodule init --recursive
$ git submodule update --recursive
```

When `trixta_space` needs to be updated you can do the following...

```sh
$ git submodule update --recursive
```

To use a specific branch or tag of the `trixta_space` app you can `cd` into
`trixta_space` and use normal git commands to change to and update the required branch or
tag. This will change the `.gitmodules` file to the use that branch or tag and
if you want to persist the change you should commit the change to that file.


### 2. Apply Patch(es)

One or more patches need to be applied to the `trixta_space` git submodule
**before** trying to build the firmware image. The patches can be applied as
follows...

Change into the `trixta_space` directory...

```sh
$ cd trixta_space
```

Check that the patch will apply cleanly...

```sh
$ git apply --check ../patches/remove_trixa_unix_library.patch
```

Apply the patch...

```sh
$ git apply ../patches/remove_trixa_unix_library.patch
```

Change back into the `trixta_firmware` directory...

```sh
$ cd -
```

### 3. Compile `trixta_space` Assets

Next, compile the `trixta_space` assets as follows...

Change into the `trixta_space` assets directory...

```sh
$ cd trixta_space/assets
```

Build assets in production mode...

```sh
$ npm install && ./node_modules/brunch/bin/brunch build --production
```

Change into the `trixta_space` directory and digest the assets...

```sh
$ cd ..
$ mix phx.digest
```

Change back into the `trixta_firmware` directory...

```sh
$ cd -
```

> Once the submodule is updated and the patches are applied you can continue.

## Targets

Nerves applications produce images for hardware targets based on the
`MIX_TARGET` environment variable. If `MIX_TARGET` is unset, `mix` builds an
image that runs on the host (e.g., your laptop). This is useful for executing
logic tests, running utilities, and debugging. Other targets are represented by
a short name like `rpi3` that maps to a Nerves system image for that platform.
All of this logic is in the generated `mix.exs` and may be customized. For more
information about targets see:

https://hexdocs.pm/nerves/targets.html#content

## Getting Started

To start your Nerves app:
  * `export MIX_TARGET=my_target` or prefix every command with
    `MIX_TARGET=my_target`. For example, `MIX_TARGET=trixta_rpi3`
  * Install dependencies with `mix deps.get`
  * Create firmware with `mix firmware`
  * Burn to an SD card with `mix firmware.burn`

### TrixtaSpace Specific Helpers

To build `trixta_space` and create the firmware image:
```sh
$ ./build.sh
```

To push updated firmware to a **running** device over ssh:
```sh
$ ./upload.sh
```

## Access TrixtaSpace App

To view the current `trixta_space` web index open the following URL in a
browser on the same network...

http://trixta.local:4000/

## Verify `trixta_rpi3` Host Apps Installation

```sh
$ ssh trixta.local
ex(trixta_firmware@trixta.local)1> cmd "git --version"
git version 2.22.0
0
```

## Learn more

  * Official docs: https://hexdocs.pm/nerves/getting-started.html
  * Official website: https://nerves-project.org/
  * Forum: https://elixirforum.com/c/nerves-forum
  * Discussion Slack elixir-lang #nerves ([Invite](https://elixir-slackin.herokuapp.com/))
  * Source: https://github.com/nerves-project/nerves

## Contributing

MRs accepted. See [CONTRIBUTING](./CONTRIBUTING.md).

## License

© Trixta Inc. See [LICENSE](./LICENSE).
