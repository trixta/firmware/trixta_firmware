# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- NA

### Updated

- NA

### Fixed

- NA

## [0.0.1] - 2019-11-12

### Added

- Initial biolerplate files and documentation

### Updated

- NA

### Fixed

- NA
