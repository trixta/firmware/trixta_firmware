# Configuration for the host (no LEDs to blink)
use Mix.Config

config :trixta_firmware, led_list: []
config :nerves_leds, names: []
