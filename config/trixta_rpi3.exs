# Configuration for the Trixta Raspberry Pi 3 (target trixta_rpi3)
use Mix.Config

config :trixta_firmware, led_list: [:green]
config :nerves_leds, names: [green: "led0"]
