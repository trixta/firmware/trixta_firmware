# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :trixta_space, TrixtaSpaceWeb.Endpoint,
  http: [port: System.fetch_env!("TRIXTA_SPACE_PORT")],
  url: [
    host: System.fetch_env!("HOSTNAME"),
    port: System.fetch_env!("TRIXTA_SPACE_PORT")
  ],
  ws_url:
    "ws://#{System.fetch_env!("HOSTNAME")}:#{System.fetch_env!("TRIXTA_SPACE_PORT")}/socket/websocket",
  check_origin: false,
  cache_static_manifest: "priv/static/cache_manifest.json",
  secret_key_base: System.fetch_env!("SECRET_KEY_BASE"),
  server: true,
  root: Path.dirname(__DIR__),
  render_errors: [view: TrixtaSpaceWeb.ErrorView, accepts: ~w(html json)],
  # pubsub: [name: Nerves.PubSub, adapter: Phoenix.PubSub.PG2],
  pubsub: [name: TrixtaSpace.PubSub, adapter: Phoenix.PubSub.PG2],
  version: Application.spec(:phoenix_app, :vsn),
  code_reloader: false

# Configure params to filter from logs
config :phoenix, :filter_parameters, ["password", "secret", "token"]

# If you are doing OTP releases, you need to instruct Phoenix
# to start the server for all endpoints:
#
#     config :phoenix, :serve_endpoints, true
config :phoenix, :serve_endpoints, true

# Configure JSON library
# config :phoenix, :json_library, Jason

# Configure Joken
config :joken, default_signer: System.fetch_env!("SECRET_KEY_BASE")

# Configure trixta modules
config :trixta_spaces, :environment, Mix.env()
config :trixta_roles, :environment, Mix.env()
config :trixta_metrics, :environment, Mix.env()
config :trixta_flows, :environment, Mix.env()
config :trixta_storage, :environment, Mix.env()
config :trixta_utils, :environment, Mix.env()
config :trixta_agents, :environment, Mix.env()

config :trixta_agents, :root_agent_password, System.fetch_env!("ROOT_AGENT_PWD")

config :trixta_metrics, TrixtaMetrics.ElasticsearchCluster,
  url: System.fetch_env!("ELASTICSEARCH_DOMAIN"),
  api: Elasticsearch.API.HTTP,
  json_library: Poison,
  default_options: [timeout: 30_000, recv_timeout: 30_000]

# Config mail
config :trixta_library, TrixtaLibrary.Mail.Mailer,
  adapter: Bamboo.SesAdapter,
  ex_aws: [region: "eu-west-1"],
  # Currently used when populating email templates. Can be pulled from the env variable TRIXTA_DOMAIN_URL
  trixta_domain_url: "https://analytics.trixta.com",
  # Currently used when populating email templates. Can be pulled from the env variable TRIXTA_SPACE_URL
  trixta_space_url: "wss://api-production.uat.analytics.trixta.io/socket",
  trixta_feedback_email: "feedback@trixta.com",
  trixta_bounce_email: "no-reply@trixta.com",
  trixta_complaints_email: "feedback@trixta.com"

config :tzdata, :data_dir, "/var/tmp"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
# import_config "#{Mix.env()}.exs"
