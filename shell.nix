let
  # Look here for information about how to generate `nixpkgs-version.json`.
  #  → https://nixos.wiki/wiki/FAQ/Pinning_Nixpkgs
  pinnedVersion = builtins.fromJSON (builtins.readFile ./.nixpkgs-version.json);
  pinnedPkgs = import (builtins.fetchGit {
    inherit (pinnedVersion) url rev;

    ref = "nixos-unstable";
  }) {};
in

# This allows overriding pkgs by passing `--arg pkgs ...`
{ pkgs ? pinnedPkgs }:

with pkgs;

mkShell {
  buildInputs = [
    # put packages here.
    stdenv
    gmp
    musl
    python2
    automake
    libtool
    autoconf
    gcc
    binutils
    openssl
    gitFull
    beam.packages.erlangR22.elixir
    nodejs
    yarn
    inotify-tools
    fwup
    squashfsTools
    file
  ];
}
