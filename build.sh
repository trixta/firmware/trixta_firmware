#!/bin/sh

cd trixta_space/assets
npm install && ./node_modules/brunch/bin/brunch build --production
cd ..
mix phx.digest
cd ..
mix do deps.get, firmware
